# Factory News

Factory News is an Android app to see the latest news which are fetched from another REST API. You can save to read the news later and of course delete news from the saved section. App also lets you redirect to detailed news article. App caches fetched articles to local database so you can read while offline.

To download .apk file, click [here](https://gitlab.com/zagi031/factory-news/-/blob/main/factory-news.apk).

Technologies used: Android, Kotlin, RecyclerView, Room, Koin, Navigation Component, Retrofit, Coroutines, Viewpager
