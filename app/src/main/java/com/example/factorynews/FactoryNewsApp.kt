package com.example.factorynews

import android.app.Application
import android.content.Context
import com.example.factorynews.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class FactoryNewsApp : Application() {
    companion object {
        lateinit var context: Context
    }

    override fun onCreate() {
        super.onCreate()
        context = this

        startKoin {
            androidContext(this@FactoryNewsApp)
            modules(viewModelModule)
        }
    }
}