package com.example.factorynews.di

import com.example.factorynews.models.repositories.ArticlesRepository
import com.example.factorynews.models.repositories.ArticlesRepository.Companion.NEWSAPI_BASE_URL
import com.example.factorynews.models.repositories.retrofit.NewsAPI
import com.example.factorynews.models.repositories.room.ArticleRoomDatabase
import com.example.factorynews.ui.articlesFragment.ArticlesViewModel
import com.example.factorynews.ui.detailedArticleFragment.DetailedArticleViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val viewModelModule = module {
    viewModel { ArticlesViewModel(get()) }
    viewModel { DetailedArticleViewModel(get()) }
    single {
        Retrofit.Builder().baseUrl(NEWSAPI_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create()).build()
            .create(NewsAPI::class.java)     // crash after this line when offline
    }
    single { ArticleRoomDatabase.getInstance().articleDao() }
    single { ArticlesRepository(get(), get()) }
}