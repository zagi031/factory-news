package com.example.factorynews.interfaces

interface ArticleItemInteractionListener {
    fun onArticleClick(position: Int)
}