package com.example.factorynews.interfaces

interface ButtonShowNewsOnWebInteractionListener {
    fun onButtonClick(url: String)
}