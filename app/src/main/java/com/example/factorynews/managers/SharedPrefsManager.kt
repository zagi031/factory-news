package com.example.factorynews.managers

import android.content.Context
import com.example.factorynews.FactoryNewsApp

class SharedPrefsManager {
    companion object {
        const val SHAREDPREFS_FILE = "factorynewsapp_sharedprefs"
        const val LASTUPDATE = "lastupdate"
    }

    private val sharedPreferences =
        FactoryNewsApp.context.getSharedPreferences(SHAREDPREFS_FILE, Context.MODE_PRIVATE)

    fun saveLastUpdate(currentTimeMillis: Long) =
        sharedPreferences.edit().putLong(LASTUPDATE, currentTimeMillis).apply()

    fun getLastUpdate() = sharedPreferences.getLong(LASTUPDATE, 0)

}