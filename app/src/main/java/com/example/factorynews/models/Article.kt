package com.example.factorynews.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "articles")
data class Article(
    @PrimaryKey
    val url: String,
    val author: String,
    val description: String,
    val title: String,
    val urlToImage: String
)

fun Article.toArticleUI() = ArticleUI(
    author = author,
    description = description,
    title = title,
    url = url,
    urlToImage = urlToImage
)

@Entity(tableName = "favorite_articles")
data class FavoriteArticle(@PrimaryKey val url: String)