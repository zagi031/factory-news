package com.example.factorynews.models

data class ArticleUI(
    val author: String,
    val description: String,
    val title: String,
    val url: String,
    val urlToImage: String
)