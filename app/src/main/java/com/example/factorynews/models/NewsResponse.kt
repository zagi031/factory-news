package com.example.factorynews.models

data class NewsResponse(
    val articles: List<Article>,
    val sortBy: String,
    val source: String,
    val status: String
)