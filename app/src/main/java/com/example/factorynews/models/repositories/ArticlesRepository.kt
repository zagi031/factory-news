package com.example.factorynews.models.repositories

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.factorynews.FactoryNewsApp
import com.example.factorynews.R
import com.example.factorynews.managers.SharedPrefsManager
import com.example.factorynews.models.Article
import com.example.factorynews.models.FavoriteArticle
import com.example.factorynews.models.SingleLiveEvent
import com.example.factorynews.models.repositories.retrofit.NewsAPI
import com.example.factorynews.models.repositories.room.ArticleDao

class ArticlesRepository(private val newsApi: NewsAPI, private val articleDao: ArticleDao) {
    private val sharedPrefsManager = SharedPrefsManager()
    private val REFRESH_RATE: Long = 300000 // 5 minutes, 300000 millis
    private val NEWSAPI_API_KEY = "6946d0c07a1c4555a4186bfcade76398"

    companion object {
        val NEWSAPI_BASE_URL = "https://newsapi.org/v1/"
    }

    private val _errorMessage = SingleLiveEvent<String>()
    val errorMessage: LiveData<String> = _errorMessage

    suspend fun getArticles(): LiveData<List<Article>> {
        val timeNow = System.currentTimeMillis()
        val timeOfLastUpdate = sharedPrefsManager.getLastUpdate()
        if (timeNow - timeOfLastUpdate > REFRESH_RATE) {
            getFreshNews()
        }
        return getStoredNews()
    }

    private fun getStoredNews() = articleDao.getArticles()

    private suspend fun getFreshNews() {
        val queries =
            mapOf("source" to "bbc-news", "sortBy" to "top", "apiKey" to NEWSAPI_API_KEY)
        try {
            val articlesResponse = newsApi.getNews(queries)
            if (articlesResponse.isSuccessful) {
                sharedPrefsManager.saveLastUpdate(System.currentTimeMillis())
                storeArticles(articlesResponse.body()?.articles!!)
            }
        } catch (t: Throwable) {
            _errorMessage.postValue(FactoryNewsApp.context.resources.getString(R.string.error_text))
        }
    }

    fun getFavoriteArticlesURLs() = articleDao.getFavoriteArticlesURLs()

    private suspend fun storeArticles(articles: List<Article>) = articleDao.insertArticles(articles)

    fun getFavorites() = articleDao.getFavoriteArticles()

    suspend fun saveToFavorites(favoriteArticle: FavoriteArticle) =
        articleDao.saveToFavorites(favoriteArticle)

    suspend fun removeFromFavorites(url: String) = articleDao.removeFromFavorites(url)

    suspend fun isArticleFavorite(url: String) = articleDao.isArticleFavorite(url) > 0
}