package com.example.factorynews.models.repositories.retrofit

import com.example.factorynews.models.NewsResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface NewsAPI {
    @GET("articles")
    suspend fun getNews(@QueryMap options: Map<String, String>): Response<NewsResponse>
}

// coroutines exception hanlder