package com.example.factorynews.models.repositories.room

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.factorynews.models.Article
import com.example.factorynews.models.FavoriteArticle

@Dao
interface ArticleDao {

    @Query("SELECT * FROM articles")
    fun getArticles(): LiveData<List<Article>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertArticles(articles: List<Article>)

//    @Query("DELETE FROM articles")
//    suspend fun deleteAllArticles()

    @Query("SELECT COUNT(url) FROM favorite_articles WHERE url = :url")
    suspend fun isArticleFavorite(url: String): Int

    @Query("SELECT * FROM favorite_articles")
    fun getFavoriteArticlesURLs(): LiveData<List<FavoriteArticle>>

    @Query("SELECT * FROM articles WHERE url IN (SELECT * FROM favorite_articles)")
    fun getFavoriteArticles(): LiveData<List<Article>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveToFavorites(article: FavoriteArticle)   // article URL is the primary key

    @Query("DELETE FROM favorite_articles WHERE url = :articleURL")
    suspend fun removeFromFavorites(articleURL: String)
}