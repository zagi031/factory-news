package com.example.factorynews.models.repositories.room

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.factorynews.FactoryNewsApp
import com.example.factorynews.models.Article
import com.example.factorynews.models.FavoriteArticle

@Database(entities = [Article::class, FavoriteArticle::class], version = 5)
abstract class ArticleRoomDatabase : RoomDatabase() {

    abstract fun articleDao(): ArticleDao

    companion object {
        private const val NAME = "article_database"
        private var instance: ArticleRoomDatabase? = null

        fun getInstance(): ArticleRoomDatabase {
            if (instance == null) {
                instance = Room.databaseBuilder(
                    FactoryNewsApp.context,
                    ArticleRoomDatabase::class.java,
                    NAME
                ).allowMainThreadQueries().fallbackToDestructiveMigration().build()
            }
            return instance as ArticleRoomDatabase
        }
    }
}