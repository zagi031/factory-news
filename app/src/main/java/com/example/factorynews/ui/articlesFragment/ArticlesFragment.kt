package com.example.factorynews.ui.articlesFragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.factorynews.R
import com.example.factorynews.databinding.FragmentArticlesBinding
import com.example.factorynews.interfaces.ArticleItemInteractionListener
import org.koin.androidx.viewmodel.ext.android.viewModel


class ArticlesFragment : Fragment(), ArticleItemInteractionListener {
    private lateinit var binding: FragmentArticlesBinding
    private val viewModel by viewModel<ArticlesViewModel>()
    private lateinit var articlesAdapter: ArticlesRecyclerViewAdapter
    private val args: ArticlesFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentArticlesBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.progressbar.visibility = View.VISIBLE

        setupRecyclerView()
        viewModel.getArticles(args.isFavoriteArticlesUseCase).observe(viewLifecycleOwner, {
            articlesAdapter.setArticles(it)
            binding.progressbar.visibility = View.GONE
        })
        viewModel.errorMessage.observe(viewLifecycleOwner, {
            AlertDialog.Builder(activity as AppCompatActivity)
                .setTitle(resources.getString(R.string.error_title))
                .setMessage(it)
                .setNegativeButton(resources.getString(R.string.error_action_text), null)
                .create().show()
        })
    }

    private fun setupRecyclerView() {
        binding.apply {
            articlesAdapter =
                ArticlesRecyclerViewAdapter(this@ArticlesFragment)
            rvArticles.apply {
                adapter = this@ArticlesFragment.articlesAdapter
                layoutManager =
                    LinearLayoutManager(
                        this@ArticlesFragment.parentFragment?.context,
                        RecyclerView.VERTICAL,
                        false
                    )
                addItemDecoration(ArticleItemDecorator(10, 10))
            }
        }
    }

    override fun onArticleClick(position: Int) = findNavController().navigate(
        ArticlesFragmentDirections.actionArticlesFragmentToDetailedArticleFragment(
            position,
            args.isFavoriteArticlesUseCase
        )
    )
}