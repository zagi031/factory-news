package com.example.factorynews.ui.articlesFragment

import android.graphics.Rect
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.factorynews.databinding.ArticleHolderBinding
import com.example.factorynews.interfaces.ArticleItemInteractionListener
import com.example.factorynews.models.ArticleUI
import com.squareup.picasso.Picasso

class ArticlesRecyclerViewAdapter(private val listener: ArticleItemInteractionListener?) :
    RecyclerView.Adapter<ArticleViewHolder>() {
    private val articles = mutableListOf<ArticleUI>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ArticleViewHolder(
        ArticleHolderBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
    )

    override fun onBindViewHolder(holder: ArticleViewHolder, position: Int) =
        holder.bind(articles[position], listener)

    override fun getItemCount() = articles.size

    fun setArticles(articles: List<ArticleUI>) {
        this.articles.clear()
        this.articles.addAll(articles)
        this.notifyDataSetChanged()
    }

}

class ArticleViewHolder(private val binding: ArticleHolderBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bind(article: ArticleUI, listener: ArticleItemInteractionListener?) {
        binding.apply {

            Picasso.get().load(article.urlToImage).into(ivArticleImage)
            tvArticleTitle.text = article.title
            tvArticleDescription.text = article.description

            root.setOnClickListener {
                listener?.onArticleClick(adapterPosition)
            }
        }
    }
}

class ArticleItemDecorator(
    private val verticalSpacingHeight: Int,
    private val horizontalSpacingWidth: Int
) : RecyclerView.ItemDecoration() {
    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        super.getItemOffsets(outRect, view, parent, state)
        outRect.offset(verticalSpacingHeight, horizontalSpacingWidth)
    }
}