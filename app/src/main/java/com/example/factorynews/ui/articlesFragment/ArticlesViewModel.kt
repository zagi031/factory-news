package com.example.factorynews.ui.articlesFragment

import androidx.lifecycle.*
import com.example.factorynews.models.ArticleUI
import com.example.factorynews.models.repositories.ArticlesRepository
import com.example.factorynews.models.toArticleUI
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking

class ArticlesViewModel(private val repository: ArticlesRepository) : ViewModel() {

    val errorMessage: LiveData<String> = repository.errorMessage

    fun getArticles(isFavoriteArticlesUseCase: Boolean): LiveData<List<ArticleUI>> {
        val result = viewModelScope.async(Dispatchers.IO) {
            if (isFavoriteArticlesUseCase) repository.getFavorites() else repository.getArticles()
        }
        return runBlocking {
            Transformations.map(result.await()) { articles ->
                articles.map { article ->
                    article.toArticleUI()
                }
            }
        }
    }
}