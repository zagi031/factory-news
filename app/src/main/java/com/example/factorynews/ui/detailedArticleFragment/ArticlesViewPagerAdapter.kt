package com.example.factorynews.ui.detailedArticleFragment

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.factorynews.databinding.ItemViewpagerBinding
import com.example.factorynews.interfaces.ButtonShowNewsOnWebInteractionListener
import com.example.factorynews.models.ArticleUI
import com.squareup.picasso.Picasso

class ArticlesViewPagerAdapter(private val listener: ButtonShowNewsOnWebInteractionListener?) :
    RecyclerView.Adapter<ArticlesViewPagerAdapter.ViewPagerViewHolder>() {
    private val articles = mutableListOf<ArticleUI>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewPagerViewHolder(
            ItemViewpagerBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ), listener
        )

    override fun onBindViewHolder(holder: ViewPagerViewHolder, position: Int) {
        holder.bind(this.articles[position])
    }

    override fun getItemCount() = articles.size

    fun setArticles(articles: List<ArticleUI>) {
        this.articles.clear()
        this.articles.addAll(articles)
        this.notifyDataSetChanged()
    }

    fun getArticle(position: Int) = articles[position]

    inner class ViewPagerViewHolder(
        private val binding: ItemViewpagerBinding,
        private val listener: ButtonShowNewsOnWebInteractionListener?
    ) :
        RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bind(article: ArticleUI) {
            binding.apply {
                Picasso.get().load(article.urlToImage).into(ivImage)
                tvTitle.text = article.title
                tvAuthor.text = "Author: ${article.author}"
                tvDescription.text = article.description

                btnShowArticleOnWeb.setOnClickListener {
                    listener?.onButtonClick(article.url)
                }
            }
        }
    }
}