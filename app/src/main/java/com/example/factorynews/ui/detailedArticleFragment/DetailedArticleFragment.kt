package com.example.factorynews.ui.detailedArticleFragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.viewpager2.widget.ViewPager2
import com.example.factorynews.databinding.FragmentDetailedArticleBinding
import com.example.factorynews.interfaces.ButtonShowNewsOnWebInteractionListener
import org.koin.androidx.viewmodel.ext.android.viewModel

class DetailedArticleFragment : Fragment(), ButtonShowNewsOnWebInteractionListener {
    private lateinit var binding: FragmentDetailedArticleBinding
    private val viewModel by viewModel<DetailedArticleViewModel>()
    private val articlesAdapter =
        ArticlesViewPagerAdapter(this)
    private val args: DetailedArticleFragmentArgs by navArgs()

    companion object {
        private const val SAVESTATE_POSITION_KEY = "SAVESTATE_POSITION_KEY"
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentDetailedArticleBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.getArticles(args.isShowingFavorites).observe(viewLifecycleOwner, {
            articlesAdapter.setArticles(it)
            binding.viewPager.setCurrentItem(
                savedInstanceState?.getInt(SAVESTATE_POSITION_KEY) ?: args.position, false
            )
            if (args.isShowingFavorites && it.isEmpty()) findNavController().navigate(
                DetailedArticleFragmentDirections.actionDetailedArticleFragmentToFavoriteArticlesFragment()
            )
            viewModel.getFavoriteArticlesURLs().observe(viewLifecycleOwner, {
                updateFAB()
            }) // while on all stored articles we need to know about changes on favorite articles table to update the fab
        })
        viewModel.fabIconId.observe(viewLifecycleOwner, { fabIconID ->
            binding.fabToggleFavorite
                .setImageDrawable(ContextCompat.getDrawable(requireContext(), fabIconID))
        })

        binding.apply {
            viewPager.adapter = articlesAdapter
            val onPageChangeCallback = object :
                ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    super.onPageSelected(position)
                    updateFAB()
                    updateAppBarTitle()
                }
            }
            viewPager.registerOnPageChangeCallback(onPageChangeCallback)
            fabToggleFavorite.setOnClickListener {
                val article = articlesAdapter.getArticle(binding.viewPager.currentItem)
                viewModel.toggleFavoriteArticle(article)
            }
        }
    }

    private fun updateFAB() {
        if (articlesAdapter.itemCount > 0) {
            viewModel.updateFABIcon(articlesAdapter.getArticle(binding.viewPager.currentItem))
        }
    }

    private fun updateAppBarTitle() {
        if (articlesAdapter.itemCount > 0) {
            (activity as AppCompatActivity).supportActionBar?.title =
                articlesAdapter.getArticle(binding.viewPager.currentItem).title
        }
    }

    override fun onButtonClick(url: String) =
        startActivity(Intent(Intent.ACTION_VIEW).apply { data = Uri.parse(url) })

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(SAVESTATE_POSITION_KEY, binding.viewPager.currentItem)
    }
}