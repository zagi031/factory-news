package com.example.factorynews.ui.detailedArticleFragment

import androidx.lifecycle.*
import com.example.factorynews.R
import com.example.factorynews.models.ArticleUI
import com.example.factorynews.models.FavoriteArticle
import com.example.factorynews.models.repositories.ArticlesRepository
import com.example.factorynews.models.toArticleUI
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class DetailedArticleViewModel(private val repository: ArticlesRepository) : ViewModel() {

    private val _fabIconId = MutableLiveData<Int>()
    val fabIconId = _fabIconId
    fun updateFABIcon(article: ArticleUI) {
        val iconId =
            if (isArticleFavorite(article)) R.drawable.ic_delete else R.drawable.ic_favorite
        _fabIconId.postValue(iconId)
    }

    fun getArticles(isFavoriteArticlesUseCase: Boolean): LiveData<List<ArticleUI>> {
        val result = viewModelScope.async(Dispatchers.IO) {
            if (isFavoriteArticlesUseCase) repository.getFavorites() else repository.getArticles()
        }
        return runBlocking {
            Transformations.map(result.await()) { articles ->
                articles.map { article ->
                    article.toArticleUI()
                }
            }
        }
    }

    fun getFavoriteArticlesURLs() = repository.getFavoriteArticlesURLs()

    fun toggleFavoriteArticle(articleUI: ArticleUI) =
        if (isArticleFavorite(articleUI)) removeFromFavorites(articleUI.url)
        else saveToFavorites(FavoriteArticle(articleUI.url))

    private fun isArticleFavorite(articleUI: ArticleUI): Boolean {
        val result = viewModelScope.async(Dispatchers.IO) {
            repository.isArticleFavorite(articleUI.url)
        }
        return runBlocking { result.await() }
    }

    private fun saveToFavorites(article: FavoriteArticle) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.saveToFavorites(article)
        }
    }

    private fun removeFromFavorites(articleURL: String) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.removeFromFavorites(articleURL)
        }
    }

}